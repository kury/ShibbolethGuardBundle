# ShibbolethGuardBundle

## Introduction
Shibboleth is a Single-Sign-On system made for webservices. This bundle provide
a Guard authenticator to use this authentication method in Symfony 5.2.
- Symfony Version 4.2 you should see
https://gitlab.com/kury/ShibbolethGuardBundle/tree/symfony4 (other branch).
- Symfony Version 3.4 you should see
https://gitlab.com/kury/ShibbolethGuardBundle/tree/symfony34 (other branch).
- Symfony Version 2.8 you should see
https://github.com/roenschg/ShibbolethGuardBundle.
- Symfony previous Version 2.8 you should see
https://github.com/roenschg/ShibbolethBundle.


## Installation
This library can be install using composer.

```
composer config repositories.shibboleth vcs https://gitlab.com/kury/ShibbolethGuardBundle
composer require gauss-allianz/shibboleth-guard-bundle:dev-master
```


## How to test locally (not test with symfony 5)
Setup a local IdP/SP infrastructure is a very time consuming job. I will describe
an easy way to test your functionality on your local system without an software
required. But you may consider using the following library:
https://packagist.org/packages/mrclay/shibalike

First of all you need to know which information are sent in which way by the
Apache2 webserver to the PHP. This might differ by several configurations.
You can test which information you get by just putting a script to your webserver
like this. (The script must be shibobleth secured area!)

```
<?php
    print_r(\$\_SERVER);
```

You can call the script and might need to configure the attribute naming. (see
section "configuration"). If the variables use the prefix "HTTP_" then skip the
prefix in the configuration and use the option "use_header".

Afterwards you can simulate the enviroment by copy the output from the php
script above and putting it into the app/app_dev.php like in the following
example.

```
    $_SERVER['HTTP_mail'] = "name@domain.tld";
    $_SERVER['HTTP_Shib-Application-Id'] = 'TEST';
    $_SERVER['HTTP_sn'] = "Lastname";
    $_SERVER['HTTP_givenName'] = "Firstname";
```

## Configuration
### User environnement
Set with Symfony doc an environnement for local user authentication (with form_login): [see official documentation](https://symfony.com/doc/5.2/security.html)

Caution:
* password must be nullable
* User class must extends UserShibboleth. You can find an [example here](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/src/Entity/User.php)

### Define a User Provider
In src\Security, create file UserProvider.php. It must extends UserShibbolethProvider.

You can find [here an example](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/src/Security/UserProvider.php) to adapt for our context.

### Set parameters
Create a new configuration file config\packages\shibboleth_guard.yaml. See file in [Example repository](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/config/packages/shibboleth_guard.yaml)

* `app_user_security.security.user_provider`: your UserProvider class
* The `app_user_security.shibboleth_logout_url` is your logout url from your Shibboleth idp provider
* Change field `app_user_security.shibboleth_username_attribute` with your unique ID field.
* The `username_attribute` Defines the meta attribute that will be used to identify the user (for the user provider)
* With the `attribute_definitions` you can overwrite several options if they are attribute names are different in your shibboleth configuration than default. You can add other attribute if you need.

### Change the security
In config\packages\security.yml, add:
* a new provider shibboleth
```yml
providers:
    ...
    #example for app_user_provider
    app_user_provider:
        entity:
            class: AppBundle:User
            property: authId
    shib_provider:
        id: app_user_security.shib_user_provider
    ...
```
* a firwall configuration
```yml
firewalls:
    ...
    main:
        lazy: true
        provider: app_user_provider
        guard:
            authenticators:
                - shibboleth_guard
            provider: shib_provider
        entry_point: shibboleth_guard
        # https://symfony.com/doc/current/security/form_login_setup.html
        form_login:
            login_path: login
            check_path: login
            enable_csrf: true
            use_referer: true
            remember_me:    false   
        logout:
            invalidate_session: true
            path: logout  
        logout_on_user_change: true
    ...
```
Important: you have to specify a `guard` entry, with variables in config.yml

The `use_headers` optiones specifies in which type the shibboleth daemon set the attributes for the php script

* adapt access control
```yml
access_control:
    - { path: ^/, roles: IS_AUTHENTICATED_ANONYMOUSLY }
    - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
    ...
```

You can find an example of [security file here](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/config/packages/security.yaml).


In this example, we allow:
* everybody to access to login page (and public space)
* a login for local users
* a login for shibboleth users

### Create logout listener
To totaly disconnect the user from shibboleth, you have to create a file in src\EventListener\LogoutListener.php.

You can find [here an example](https://gitlab.com/kury/ShibbolethGuardBundle/-/blob/master/Example/src/EventListener/LogoutListener.php) that you should adapt to your context.

Declare listener in the services.yaml file

```yml
services:
    ...
    #example for logout listener
    App\EventListener\LogoutListener:
        tags:
            - name: kernel.event_subscriber
              dispatcher: security.event_dispatcher.main
    ...
```

### Create Security controller
Create file src\Controller\SecurityController.php. You can find [an example here](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/src/Controller/SecurityController.php), that you can apapt.

This file will manage:
* login actions
* logout actions
* redirect to your shibboleth special webpage

### create a login page
If you allow local user authentication, you need a login page, with a button for shibboleth authentication. [You can get an example here](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/templates/security/login.html.twig)

templates\security\login.html.twig

### create a shibboleth page
Be carreful: in this example the url /shib must be under shibboleth requirement in your apache configuration. We will create a page that will never be displayed.
[Example of a file templates\security\shib.html.twig](https://gitlab.com/kury/ShibbolethGuardBundle/blob/master/Example/templates/security/shib.html.twig)

## Shibboleth - Apache configuration
In your apache configuration page, you need to request a shibboleth session on the following url: /shib

For example:
```shell
<Location /myapp/index.php/shib>
    AuthType shibboleth
    ShibRequireSession on
    require shibboleth
    ShibUseHeaders on
</Location>
```
Don't forget to reload your web server.



You may have all elements, if you need more details fell free to contact me!
