<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * SecurityController
 * Manage login and logout actions
 *
 * @author kury
 */
class SecurityController extends AbstractController {

    /**
     * Try to authenticate user,
     * Redirect user to login page if error or if no credential
     * @Route("/login", name="login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
                    'last_username' => $lastUsername,
                    'error' => $error,
        ]);
    }

    /**
     * Logout actions
     * @Route("/logout", name="logout", methods={"GET"})
     */
    public function logout() {
        // controller can be blank: it will never be executed!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     * Display shibboleth page
     * the url /shib must be under shibboleth requirement in your apache configuration
     * @Route("/shib", name="shib", methods={"GET"})
     */
    public function shibbolethRedirectAction() {
        if (!$this->getUser()) {
            return $this->render('security/shib.html.twig');
        }
        return $this->redirectToRoute('home');
    }

}
