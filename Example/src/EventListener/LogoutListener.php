<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Description of LogoutListener
 *
 * @author kury
 */
class LogoutListener implements EventSubscriberInterface {

    private $container;

    function __construct(ParameterBagInterface $container) {
        $this->container = $container;
    }

    public function onLogout(LogoutEvent $event) {
            
        // redirect the user to where they were before the login process begun.
        if ($event->getRequest()->getHost() != 'localhost') {
            // Redirect url and seconds (window.location)
            $referer_url = $this->container->get('shibboleth_logout_url');
            if ($referer_url) {
                $event->setResponse(new RedirectResponse($referer_url));
                return new RedirectResponse($referer_url);
            }
        } else {
            $referer_url = str_replace('logout', 'login', $event->getRequest()->getUri());
            return new RedirectResponse($referer_url);
        }
        return new RedirectResponse($this->route->generate('home'));
    }

    public static function getSubscribedEvents(): array {
        return [
            LogoutEvent::class => 'onLogout',
        ];
    }

}
