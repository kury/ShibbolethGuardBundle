<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use GaussAllianz\ShibbolethGuardBundle\Security\User\UserShibboleth;

/**
 * @ORM\Entity
 */
class User extends UserShibboleth {

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $authId;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=200, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastlog", type="datetime", nullable=false)
     */
    private $lastlog;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="temoinauteur", type="string", length=100, nullable=false)
     */
    private $temoinauteur;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="temoindate", type="datetime", nullable=false)
     */
    private $temoindate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     */
    private $isActive = '1';

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function getId(): ?int {
        return $this->id;
    }

    public function getAuthId(): ?string {
        return $this->authId;
    }

    public function setAuthId(string $authId): self {
        $this->authId = $authId;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string {
        return (string) $this->authId;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string {
        return (string) $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials() {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    function getFirstName(): string {
        return $this->firstName;
    }

    function getLastName(): string {
        return $this->lastName;
    }

    function getEmail() {
        return $this->email;
    }

    function getLastlog(): \DateTime {
        return $this->lastlog;
    }

    function getTemoinauteur(): string {
        return $this->temoinauteur;
    }

    function getTemoindate(): \DateTime {
        return $this->temoindate;
    }

    function getIsActive() {
        return $this->isActive;
    }

    function setFirstName(string $firstName): void {
        $this->firstName = $firstName;
    }

    function setLastName(string $lastName): void {
        $this->lastName = $lastName;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setLastlog(\DateTime $lastlog): void {
        $this->lastlog = $lastlog;
    }

    function setTemoinauteur(string $temoinauteur): void {
        $this->temoinauteur = $temoinauteur;
    }

    function setTemoindate(\DateTime $temoindate): void {
        $this->temoindate = $temoindate;
    }

    function setIsActive($isActive): void {
        $this->isActive = $isActive;
    }

    public function isActive(): bool {
        return $this->isActive;
    }

}
