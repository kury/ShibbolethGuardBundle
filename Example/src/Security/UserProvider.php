<?php

namespace App\Security;

use \GaussAllianz\ShibbolethGuardBundle\Security\User\UserShibboleth;
use \GaussAllianz\ShibbolethGuardBundle\Security\User\UserShibbolethProvider;
use \Doctrine\ORM\EntityManagerInterface;
use \App\Entity\User;

/**
 * Description of UserProvider
 *
 * @author kury
 */
class UserProvider extends UserShibbolethProvider {

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function createUser(array $credentials): UserShibboleth {
        //dump($user);die();
        // Create user object using shibboleth attributes stored in the token. 
        $user = new User();
        $user->setAuthId($credentials['uid']);
        $user->setFirstName($credentials['sn']);
        $user->setLastname($credentials['givenName']);
        $user->setEmail($credentials['mail']);
        // If you like, you can also add default roles to the user based on shibboleth attributes. E.g.:
        /* if ($credentials->isStudent()) {
          $user->addRole('ROLE_STUDENT');
          } elseif ($credentials->isStaff()) {
          $user->addRole('ROLE_STAFF');
          } else {
          $user->addRole('ROLE_GUEST');
          } */
        $user->setLastlog(new \DateTime());
        $user->setIsActive(true);
        $user->setTemoinauteur('shibboleth autologin');
        $user->setTemoindate(new \DateTime());

        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    public function afterLoginSuccess(UserShibboleth $user) {
        $user->setLastlog(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();
    }

    public function getUserByShibbolethParameters($credentials): ?UserShibboleth {
        $user = null;
        if (is_array($credentials)) {
            $user = $this->em->getRepository(User::class)->findOneByAuthId($credentials['uid']);
        } else {
            //support for old Shibboleth system
            $user = $this->em->getRepository(User::class)->findOneByAuthId($credentials);
        }
        return $user;
    }

}
