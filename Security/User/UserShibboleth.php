<?php

namespace GaussAllianz\ShibbolethGuardBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User interface to be implemented, for shibboleth authentication
 *
 * @author kury
 */
abstract class UserShibboleth implements UserInterface {

    /**
     * Get if an user is active or not, ie if he can log in or not to the application
     */
    abstract public function isActive(): bool;
}
