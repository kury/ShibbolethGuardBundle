<?php

namespace GaussAllianz\ShibbolethGuardBundle\Security\User;

use GaussAllianz\ShibbolethGuardBundle\Security\ShibbolethUserProviderInterface;
use GaussAllianz\ShibbolethGuardBundle\Security\User\UserShibboleth;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * UserProvider For Shibboleth Authentication
 *
 * @author kury
 */
abstract class UserShibbolethProvider implements ShibbolethUserProviderInterface {

    /**
     * Create a user according to shibboleth parameters
     * @param array $credentials shibboleth parameters
     * @return UserShibboleth the new User
     */
    abstract public function createUser(array $credentials): UserShibboleth;

    /**
     * Find a user according to its shibboleth attributes
     * @param type $credentials shibboleth attributes
     * @return UserShibboleth|null
     */
    abstract public function getUserByShibbolethParameters($credentials): ?UserShibboleth;

    /**
     * Make actions after shibboleth login user success
     * @param UserShibboleth $user current user
     */
    abstract public function afterLoginSuccess(UserShibboleth $user);

    /**
     * Try to find a user, and create it if not found, according to given shibboleth attributes
     * @param array|string $credentials shibboleth attributes or uid
     * @return UserInterface current authenticate user
     * @throws UnauthorizedHttpException
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($credentials): UserInterface {
        //try to get a registered user
        $user = $this->getUserByShibbolethParameters($credentials);
        if ($user) {
            if ($user->isActive()) {
                //if user is allowed, log in
                $this->afterLoginSuccess($user);
                $user->allowed = 1;
                return $user;
            }
            //user not allowed
            $user->allowed = 0;
            return $user;
        } else {
            if (!is_array($credentials)) {
                throw new UsernameNotFoundException("User " . $credentials . " not found.");
            }
            try {
                //try to create a new user
                return $this->createUser($credentials);
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
                throw new UsernameNotFoundException("User " . $credentials['uid'] . " not found.");
            }
        }
        return null;
    }

    /**
     * refresh current user: check if user exists and can log in
     * @param UserInterface $user current user
     * @return UserInterface
     * @throws UnsupportedUserException
     */
    public function refreshUser(UserInterface $user): UserInterface {
        if (!$user instanceof UserShibboleth) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Say if userporovider is the good one to manage this user instance
     * @param string $class
     * @return bool
     */
    public function supportsClass($class): bool {
        return UserShibboleth::class === $class;
    }

}
